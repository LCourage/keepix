# KeePix, un simple hébergeur de fichier

Il s'agit pricipalement d'un habillage du script d'[auto-hébergement](http://sebsauvage.net/wiki/doku.php?id=php:filehosting) créé par [sebsauvage](http://sebsauvage.net/) et modifié par [Corentin Bettiol](http://l3m.in/). Voir [son Gitlab](https://gitlab.com/sodimel/up/)


#### Installation
* Téléchargez l'archive [zippée](https://framagit.org/LCourage/keepix).
* Décompressez les fichiers
* Modifiez le mot de passe dans les deux fichiers (index.php **ET** files/index.php).
* Envoyez tout ça sur votre hébergement.
* Enjoy.




#### Commentaire de l3m
Alors **à la base**, c'était vraiment hypersimple.  
##### Mais j'ai très très vite réussi à tout compliquer, donc voici la liste des features du projet:
* Hébergement de fichiers de tous types dans le sous-dossier `files/`.
* Protection de l'hébergement par mot de passe.
* Hébergement d'images contenues dans le presse-papier en appuyant sur ctrl+v (si le mot de passe est rentré ou bien si l'utilisateur est connecté).
* Cookie de connexion d'une durée de deux heures (pour éviter de retaper le mot de passe tout le temps).
* Visionnage des fichiers hébergés par n'importe qui, pour peu qu'il/elle dispose de l'adresse dudit fichier.
* Liste des fichiers hébergés accessible par mot de passe.
* Possibilité de supprimer un fichier hébergé depuis la liste.
* Possibilité de récupérer la date d'upload des fichiers depuis la liste.
* Possibilité d'afficher la liste des fichiers du plus ancien au plus récent (et inversement).


### Modifications de LCourage:
* Styles et icônes
* Désactivation de la fonction Ctrl+V dans la zone d'envoi
* Ajout de boutons "copier le lien" et "copier le lien Markdown"
* Ajout d'une vignette dans l'infobulle d'information
* Correction : la déconnexion ne pouvait s'effectuer que depuis la page qui avait généré le cookie


#### Crédits :
* Image de fond : (a priori) http://www.london-photographic-association.com/
* Icônes : [Breeze](https://github.com/KDE/breeze-icons)
* Clipboard.js : [Zeno Rocha](https://zenorocha.github.io/clipboard.js)
* Custom-file-input.js : [Osvaldas Valutis](www.osvaldas.info)
