<?php

	// config:
	$PASSWORD='VOTRE_MOT_DE_PASSE';
	date_default_timezone_set('Europe/Paris'); // because french baguette omelette du fromage
	$url = "http" . ($_SERVER['HTTPS'] ? 's' : '') . "://{$_SERVER['HTTP_HOST']}";

	$BouttonRetour='retour';
	$Trier='trier';
	$Trier2='trier2';
	
	
	// disconnect
	if(isset($_GET['deco'])){
		setcookie('actif', '',time()-3600, "/");
		setcookie("expire","a",time()-3600, "/");
		header('Location: index.php');
	}

	// connect
	if(isset($_POST['password']) && $_POST['password'] == $PASSWORD){
		setcookie('actif', $PASSWORD, time()+60*60*2, "/"); // not very secure ?
		setcookie("expire",time()+60*60*2,time()+60*60*2, "/");
		header('Location: index.php');
	}


	if(isset($_GET['unlink']) && $_GET['unlink'] != "index.php"){
		if(isset($_COOKIE["actif"]) && $_COOKIE['actif'] == $PASSWORD){
			unlink($_GET['unlink']);
			header('Location: index.php');
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>KeePic</title>
	<meta name="viewport" content="width=device-width" />
<link rel="icon" type="image/png" href="../img/favicon.png" />
	<link rel="stylesheet" href="../design.css" />
</head>
<body>
<div class="contenu">
<?php if(isset($_COOKIE['actif'])){ echo " <div class='deconx'> <a href=\"?deco\" title='Déconnexion'>&nbsp;</a></div>"; } ?> 
	<h1>KeePic</h1>

	<?php
	if(isset($_COOKIE['actif']) && $_COOKIE['actif'] == $PASSWORD){
		echo "";
		if(!isset($_GET['antisort'])){
			echo "<div class='$Trier'><small><a href=\"?antisort\">Trier du plus ancien au plus récent</a></small></div>";
		}
		else{
			echo "<div class='$Trier2'><small><a href=\"index.php\">Trier du plus récent au plus ancien</a></small></div>";
		}
		$nb_fichier = 0;
		echo '<ul class="liste">';
		if($dossier = opendir('../files')){

			while(false !== ($fichier = readdir($dossier))){
				if($fichier != '.' && $fichier != '..' && $fichier != 'index.php'){
					$fichiers[] = $fichier;
					$nb_fichier++; // On incrémente le compteur de 1
				} // On ferme le if (qui permet de ne pas afficher index.php, etc.)
			} // On termine la boucle

			sort($fichiers);
			if(!isset($_GET['antisort'])){
				$fichiers = array_reverse($fichiers);
			}
			foreach ($fichiers as $fichier){
				echo '<li><a class="link" href="./' . $fichier . '">' . $fichier . '</a><div class="actions"><div class="actplace"><button title="Copier le lien Markdown dans le presse-papier" class="bttn2 mkd" data-clipboard-text="![Description de l\'image]('.$url.''.dirname($_SERVER['PHP_SELF']).'/' .$fichier . ')">&nbsp;</button><button title="Copier le lien dans le presse-papier" class="bttn2" data-clipboard-text="'.$url.''.dirname($_SERVER['PHP_SELF']).'/' .$fichier . '">&nbsp;</button><div class="unlink"><span class="infotext info">&nbsp;<span class="infobulle"><span class="entete">'.  date("d/m/Y \à H:i:s", filemtime($fichier)) .'</span><br /><img src="' .$fichier .'" alt="' .$fichier .'" /></span></span> <a href="index.php?unlink='.$fichier.'"><span class="close del" title="Supprimer le fichier du serveur">&nbsp;</span></a></div></div></div></li>';
			}

			echo '</ul>
			<p class="nombre">Il y a <strong>' . $nb_fichier .'</strong> fichier(s) dans le dossier.</p>';

			closedir($dossier);
		}
		else{
		   	 echo 'Le dossier n\' a pas pu être ouvert';
    	}
$BouttonRetour='retour';

    	echo "<div class='$BouttonRetour'><a href=\"../index.php\">Retour</a></div>";
    }
    else{
    	?>
    	<form method="post" action="index.php">        
    		<input type="password" name="password" class="mdp" placeholder="Mot de passe"><br>
    		<input type="submit" value="Voir les fichiers" class="envoifiles"> 
		</form>
		<p>
			<a class="retour" href="../index.php">Retour</a>
		</p>
    	<?php
    }
	?></div>
	<div class="credits"><small>Script original par <a href="http://sebsauvage.net/wiki/doku.php?id=php:filehosting">sebsauvage.net</a> et <a href="http://l3m.in/">l3m.in</a> - 
<a href="https://gitlab.com/pourrito/KeePic" target="_blank">Source</a></small></small></div>
    <script src="../dist/clipboard.js"></script>

    <!-- 3. Instantiate clipboard by passing a list of HTML elements -->
    <script>
    var btns = document.querySelectorAll('button');
    var clipboard = new Clipboard(btns);
    clipboard.on('success', function(e) {
        console.log(e);
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });
    </script>
</body>
</html>
